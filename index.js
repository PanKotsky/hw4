const NewpostRepository = require('./src/repositories/Newpost');
const express = require('express')
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3000;
const host = process.env.HOST;

app.use(cors());

app.get("/newsposts", function (req, res) {
    NewpostRepository.findAll()
        .then(result => res.status(200).send(result))
        .catch(err => res.status(500).send(err));
})

app.get("/newsposts/:id", function (req, res) {
    NewpostRepository.findById(req.params["id"])
        .then(function (result) {
            (!result) ? res.status(404).send(result) : res.status(200).send(result);
        })
        .catch(err => res.status(500).send(err));
})

app.post("/newsposts", express.json(),function (req, res) {
    const data = req.body;
    NewpostRepository.create(data)
        .then(result => res.status(200).send({id: result}))
        .catch(err => res.sendStatus(500).send(err));
})

app.put("/newsposts/:id", express.json(),function (req, res) {
    const data = req.body;
    NewpostRepository.updateById(req.params["id"], data)
        .then(function (result) {
            (!result) ? res.status(404).send(result) : res.status(200).send({id: result});
        })
        .catch(err => res.status(500).send(err));
})

app.delete("/newsposts/:id", express.json(),function (req, res) {
    NewpostRepository.deleteById(req.params["id"])
        .then(function (result) {
            (!result) ? res.status(404).send(result) : res.status(200).send(result);
        })
        .catch(err => res.status(500).send(err));
})

app.listen(port, host);