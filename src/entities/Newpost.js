"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Newpost = void 0;
class Newpost {
    constructor() {
        this.pk = 'id';
        this.table = 'newpost';
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
        return this;
    }
    getTitle() {
        return this.title;
    }
    setTitle(title) {
        this.title = title;
        return this;
    }
    getText() {
        return this.text;
    }
    setText(text) {
        this.text = text;
        return this;
    }
    getDate() {
        return this.createDate;
    }
    setDate(createDate) {
        this.createDate = createDate;
        return this;
    }
}
exports.Newpost = Newpost;
