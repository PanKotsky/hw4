"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepository = void 0;
const FileDB = require("../middleware/FileDB");
const Hydrator = require("../middleware/Hydrator");
class BaseRepository {
    constructor(schemaName) {
        this._collection = new FileDB(schemaName);
        this._hydrator = new Hydrator;
    }
    getLastRecord(data) {
        return data.length > 0 ? data[data.length - 1] : null;
    }
    create(item) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._collection.read()
                .then(dataExist => {
                let lastRecord = this.getLastRecord(dataExist);
                // 'id' - в ідеалі тут має бути визначений PK, але це ще більше заморочитись
                let newRecordId = lastRecord ? ++lastRecord['id'] : 1;
                item = this._hydrator.hydrateRecord(this._collection.schemaTable, item, newRecordId);
                this._collection.write(item);
                return newRecordId;
            })
                .catch(err => console.error(err.message));
        });
    }
    updateById(id, item) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._collection.updateById(id, item);
        });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._collection.deleteById(id);
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._collection.read();
        });
    }
    findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._collection.read()
                .then(records => {
                // @ts-ignore
                return records ? records.find(value => value.id == id) : null;
            });
        });
    }
}
exports.BaseRepository = BaseRepository;
