type TableSchema = {
    schema: {}
};

const NewpostSchema: TableSchema = {
    schema: {
        id: Number,
        title: String,
        text: String,
        createDate: Date,
    },
};

export = {
    NewpostSchema: NewpostSchema
}