"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const fs = require('fs');
const path = __importStar(require("path"));
const Newpost = {
    // @ts-ignore
    id: Number,
    // @ts-ignore
    title: String,
    // @ts-ignore
    text: String,
    // @ts-ignore
    createDate: Date,
};
const AllSchemas = {
    'Newpost': Newpost
};
class FileDB {
    constructor(table) {
        this.table = table;
        // @ts-ignore
        this.schemaTable = AllSchemas[table];
    }
    prepareDataRead(data) {
        return data ? JSON.parse(data) : [];
    }
    prepareDataWrite(data, newRecord) {
        data.push(newRecord);
        if (typeof data === 'object') {
            return JSON.stringify(data);
        }
        else {
            console.error(data);
            throw new Error('Unexpected format of data');
        }
    }
    checkFile(filename) {
        return fs.existsSync(filename);
    }
    read() {
        return __awaiter(this, void 0, void 0, function* () {
            let filename = this.getFilename();
            // створити файл за відсутності
            !this.checkFile(filename) && fs.writeFileSync(filename, '');
            return this.prepareDataRead(yield fs.promises.readFile(filename, 'utf8'));
        });
    }
    write(newRecord) {
        let filename = this.getFilename();
        !this.checkFile(filename) && fs.writeFileSync(filename, '');
        this.read()
            .then(records => {
            console.log(newRecord);
            fs.promises.writeFile(filename, this.prepareDataWrite(records, newRecord));
        })
            .catch(err => console.error(err));
    }
    updateById(id, record) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.read()
                .then(records => {
                let result = null;
                // @ts-ignore
                let index = records.findIndex(value => value.id == id);
                if (index !== -1) {
                    for (const [field, value] of Object.entries(record)) {
                        // @ts-ignore
                        records[index][field] = value;
                    }
                    result = records[index];
                    fs.promises.writeFile(this.getFilename(), JSON.stringify(records));
                }
                return result;
            })
                .catch(err => console.error(err.message));
        });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let filename = this.getFilename();
            if (!this.checkFile(filename)) {
                throw new Error('File DB doesn`t exist');
            }
            return this.read()
                .then(records => {
                let result = null;
                // @ts-ignore
                let index = records.findIndex(value => value.id === Number(id));
                if (index !== -1) {
                    records.splice(index, 1);
                    fs.promises.writeFile(this.getFilename(), JSON.stringify(records));
                    result = id;
                }
                return result;
            })
                .catch(err => console.error(err.message));
        });
    }
    getFilename() {
        return path.join(__dirname, '../db/' + this.table + '.txt');
    }
}
module.exports = FileDB;
