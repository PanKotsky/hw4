import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import NewsView from "@/views/NewsView.vue";
import FormNewsComponent from "@/components/FormNewsComponent.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "/all-news",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/IndexView.vue"),
    props: true,
  },
  {
    path: "/news/:id",
    name: "news",
    component: NewsView,
    props: true,
  },
  {
    path: "/news/add",
    name: "addNews",
    component: FormNewsComponent,
  },
  {
    path: "/news/edit/:id",
    name: "editNews",
    component: FormNewsComponent,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
